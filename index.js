require('dotenv').config();
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const HttpsProxyAgent = require('https-proxy-agent');
const schedule = require('node-schedule');
const mime = require('mime-types');
const Https = require('node:https');
const path = require('path');
const xml = require('xml');
const fs = require('fs');
const sleep = require('util').promisify(setTimeout);
const { S3Client,
  PutObjectCommand,
  ListObjectsV2Command,
  DeleteObjectCommand
 } = require('@aws-sdk/client-s3');
const { addProxyToClient } = require('aws-sdk-v3-proxy');

const EducationProvider = require('./classes/EducationProvider.js')
const EducationInfo = require('./classes/EducationInfo.js');
const { clearLine } = require('readline');
const EducationEvent = require('./classes/EducationEvent.js');
const EducatonList = require('./classes/EducationList.js');
const EventList = require('./classes/EventList.js');

const dec = { declaration: { version: '1.0', encoding: 'UTF-8' } };

const doctype = `<!DOCTYPE html>`
const isDevelopMode = process.env.PROXY_MODE == "true"? undefined: true;


const retries = 10;
const pause = 100;
let fetchOptions = {
  "headers": {
    "af-environment": "PROD",
    Accept: 'application/json'
  },
  "body": null,
  "method": "GET",
  retry: retries,
  pause: pause,
  agent: undefined
}
let Bucket;
let accessKeyId;
let secretAccessKey;
let region;
let s3Client;


try {
Bucket = process.env.AWS_BUCKET;
accessKeyId = process.env.ACCESS_KEY_ID;
secretAccessKey = process.env.SECRET_ACCESS_KEY;
region = process.env.REGION;
s3Client = addProxyToClient(new S3Client({
  region,
  credentials: {
    accessKeyId,
    secretAccessKey
  }
}));
  fetchOptions = {
    "headers": {
      "af-environment": "PROD",
      Accept: 'application/json'
    },
    "body": null,
    "method": "GET",
    retry: retries,
    pause: pause,
    agent: HttpsProxyAgent(process.env.PROXY_URL)
  }
  
} catch (error) {
  console.log("Error: missing environment variables")
  console.log(error)
}

const uploadToS3 = async (filePath) => {
  const fileContent = fs.readFileSync(filePath);
  const fileKey = filePath.substring(1);
  const type = mime.lookup(filePath);
  const params = {
    Bucket,
    Key: `${fileKey}`,
    Body: fileContent,
    ContentType: type
  };
  const command = new PutObjectCommand(params);

  try {
    const data = await s3Client.send(command);
    console.log(`uploaded file: ${fileKey} to S3 file type ${type}`);
  } catch (error) {
    console.log(error);
  }
}


const getAllFiles = function (dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath);

  arrayOfFiles = arrayOfFiles || []

  files.forEach((file) => {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
    } else {
      const filename = path.join(dirPath, "/", file);
      arrayOfFiles.push(filename);
    }
  });
  return arrayOfFiles;
}

const deleteS3BucketObject = async (objName) => {
  const params = {
    Bucket,
    Key: objName
  }
  const command = new DeleteObjectCommand(params);
  try {
    const response = await s3Client.send(command);
    console.log(`Successfully deleted ${objName} from S3`);
  } catch (error) {
    console.log(error);
  }
}

const getEventsByEducation = async (id) => {
  let events = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/tillfalle?client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}&select=*)&tlr_unique_utbildning_id=eq.${id}`, fetchOptions);

  let eventsJSON = await events.json();
  
  return eventsJSON;
};

const getEducation = async (id) => {
  let education = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/utbildning_uv_plats?&select=*,utbildningsmodul(*),utbildning_uv_plats_studiebesok(*))&tlr_unique_utbildning_id=eq.${id}&client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}`, fetchOptions);
  let educationJSON = await education.json();
  return educationJSON
};

const getEducations = async () => {

  let educationsResponse = await fetch(`https://ipf.arbetsformedlingen.se:443/tlrs-aub-for-asok/v1/rpc/utbildning_list?&limit=9999&offset=0&client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}`, fetchOptions);
  const educationList = await educationsResponse.json();

  let html = new EducatonList(educationList);
  fs.writeFile(`${process.env.OUTPUT_PATH}/utb_emil/educationlist.html`, doctype + xml(html).replace('<title>EMIL 2.0 EducationList</title>','<title>EMIL 2.0 EducationList</title><meta charset="utf-8"/>'), function (err) {
    if (err) return console.log(err);
  });
  return educationList
}

let deleteFolder = function (path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function (file) {
      let curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) {
        fs.rm(curPath, { recursive: true }, err => {
          if (err)
            console.log(err);
        });
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
  }
  console.log(`Delete Operation complete`);
};

(async function () {
  //delete old output
  let events = [];
  deleteFolder(`${process.env.OUTPUT_PATH}/utb_emil`);


    
    let path = `${process.env.OUTPUT_PATH}/utb_emil`;
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }
    try {
      fs.readFile('./static/providerlist.html', 'utf8', (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        fs.writeFile(`${process.env.OUTPUT_PATH}/utb_emil/providerlist.html`, data, function (err) {
          if (err) return console.log(err);
  
        });
      });
  
  
  
    } catch (err) {
      console.error(err)
    }
  
 


  const provider = new EducationProvider();


  fs.writeFile(`${process.env.OUTPUT_PATH}/utb_emil/provider.xml`, xml(provider, isDevelopMode), function (err) {
    if (err) return console.log(err);

  });

  console.log('Data Fetching Start');
  try {
    let educationList = await getEducations();

    for await (const element of educationList) {
      await sleep(pause);

      let education = await getEducation(element.tlr_unique_utbildning_id);
      education = education[0];
      let path = `${process.env.OUTPUT_PATH}/utb_emil/${element.tlr_unique_utbildning_id.slice(0, 3)}/${element.tlr_unique_utbildning_id}/`;

      //todo urskilja grupppstart osv
      let eventsForEducation = await getEventsByEducation(education.tlr_unique_utbildning_id);
      let hasSuccsessivStart = false;
      let hasStudietakt = false;

      if (eventsForEducation.length == 0){
        eventsForEducation.push({
          tlr_unique_utbildning_id: element.tlr_unique_utbildning_id,
          tlr_utbildningstillfalle_id: element.tlr_unique_utbildning_id+"_0200",          
          utbildningsnamn: element.utbildningsnamn,
          antagningsform: "Successiv"
        })
      
      }
      
      eventsForEducation.forEach(event => {
        if (event.antagningsform === "Successiv" && !hasSuccsessivStart && !hasStudietakt ) {
          events.push({
            tlr_unique_utbildning_id: element.tlr_unique_utbildning_id,
            tlr_unique_tillfalle_id: event.tlr_utbildningstillfalle_id,            
            utbildningsnamn: element.utbildningsnamn, studietakt: event.studietakt
          });
          let educationEvent = new EducationEvent(education, event);

          if (!fs.existsSync(path)) {
            fs.mkdirSync(`${path}/events`, { recursive: true });
          }
          fs.writeFile(`${path}/events/${event.tlr_utbildningstillfalle_id}.xml`, xml({}, dec)+ xml(educationEvent, isDevelopMode), function (err) {
            if (err) return console.log(err);

          });
          hasSuccsessivStart = event.antagningsform === "Successiv" ? true : hasSuccsessivStart;
        } else if (event.antagningsform === "Gruppstart") {
          events.push({
            tlr_unique_utbildning_id: element.tlr_unique_utbildning_id,
            tlr_unique_tillfalle_id: event.tlr_unique_tillfalle_id,
            studietakt: event.studietakt,
            utbildningsnamn: element.utbildningsnamn
          })
          let educationEvent = new EducationEvent(education, event)

          if (!fs.existsSync(path)) {
            fs.mkdirSync(`${path}/events`, { recursive: true });
          }
          fs.writeFile(`${path}/events/${event.tlr_unique_tillfalle_id}.xml`, xml({}, dec) + xml(educationEvent, isDevelopMode), function (err) {
            if (err) return console.log(err);

          });
        }
      });

      if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true });
      }
      if (!fs.existsSync(path)) {
        fs.mkdirSync(`${path}/events`, { recursive: true });
      }

      let fileName = `${path}education.xml`;
      education = new EducationInfo(education);
      fs.writeFile(fileName, xml(education, isDevelopMode), function (err) {
        if (err) return console.log(err);

      });
    }

    let html = new EventList(events);

    fs.writeFile(`${process.env.OUTPUT_PATH}/utb_emil/eventlist.html`, doctype + xml(html).replace('<title>EMIL 2.0 EventList</title>','<title>EMIL 2.0 EventList</title><meta charset="utf-8"/>'), function (err) {
      if (err) return console.log(err);

    });



  
  let listOfObjectsinS3Bucket = [];
  const getListObjectsinS3Bucket = async (Prefix, ctnToken) => {
    const params = {
      Bucket,
      Prefix,
      MaxKeys: 1000,
      ContinuationToken: ctnToken || null
    }
    const command = new ListObjectsV2Command(params);
    try {
      const response = await s3Client.send(command);
      if (!response.Contents)
        return [];
      listOfObjectsinS3Bucket = [...listOfObjectsinS3Bucket, ...response.Contents.map(content => content['Key'])];
      if (response.IsTruncated)
        return await getListObjectsinS3Bucket('utbildningar/utb_emil', response.NextContinuationToken);
      
      return listOfObjectsinS3Bucket;
    } catch (error) {
      console.log(error);
    }
  } 









      // deleting old output from S3


  
    try {
      const listOfObjects = await getListObjectsinS3Bucket('utbildningar/utb_emil');
    
      for await (const obj of listOfObjects) {
        await deleteS3BucketObject(obj);
      }
      console.log('Delete from S3 completed');
    } catch (error) {
      console.log(error);
    }
    console.log('Data Uploading to S3 start');
    await sleep(pause);
    const filesToUpload = getAllFiles(`${process.env.OUTPUT_PATH}/utb_emil`);

    for await (let file of filesToUpload) {
      await uploadToS3(file);
    }
  } catch (err) {
    console.error(err);
  }
  

  console.log("Done");

}());
