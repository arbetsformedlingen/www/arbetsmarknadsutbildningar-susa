const LangUrl = require('./LangUrl.js');
const LocationalAddress = require('./LocationalAddress.js');
const PhoneNumber = require('./PhoneNumber.js');
const PostalAddress = require('./PostalAddress.js');
const ResponsibleBody = require('./ResponsibleBody.js');
const URI = require('./URI.js');
const Name = require('./Name.js');
const ListItem = require('./ListItem');
const List = require('./List.js');
const ListBody = require('./ListBody.js');
const EducationInfo = require('./EducationInfo.js');
const { clearLine } = require('readline');
const Expires = require('./Expires.js');
const EducationEvent = require('./EducationEvent.js');
const LastEdited = require('./LastEdited')
const Identifier = require('./Identifier.js');

class EducationProvider{
    constructor(){
        const ProviderName = new Name('Arbetsförmedlingen', 'swe');
        const providerUrl = new LangUrl('https://www.arbetsformedlingen.se','swe');
        const providerVisitAddress = new LocationalAddress('Solna', '0184', 'Elektrogatan 4', 'Sverige', 17154 ,'');
        const providerContactAddress = new PostalAddress('Stockholm', 'Sverige', 11399, '','','Arbetsförmedlingen','');
        const providerEmailAddress = new URI('https://arbetsformedlingen.se/kontakt/for-arbetssokande');
        const providerPhone = new PhoneNumber('Växel', '0771-416 416');
        const providerResponsibleBody = new ResponsibleBody('C_Body', 'statlig', 'Arbetsförmedlingen', 'swe');
        const lastEdited = new LastEdited();
        const expires = new Expires();
        const identifier = new Identifier("p",'1');
        

        this.educationProvider = []
        this.educationProvider.push( {
            _attr: {
                'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema',
                xmlns: 'http://www.sis.se/ss10700/EMIL2.01'
            }
        });
        this.educationProvider.push(identifier);
        this.educationProvider.push(lastEdited);      
        this.educationProvider.push(expires);
        this.educationProvider.push(ProviderName);
        this.educationProvider.push(providerUrl);
        this.educationProvider.push(providerVisitAddress);
        this.educationProvider.push(providerContactAddress);
        this.educationProvider.push(providerEmailAddress);
        this.educationProvider.push(providerPhone);
        this.educationProvider.push(providerResponsibleBody);
    }
}
module.exports = EducationProvider