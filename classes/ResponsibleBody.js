const LangString = require("./LangString");
const Name = require("./Name");
const Type = require("./Type");

class ResponsibleBody{
    constructor(xsitype, codeStr, name, lang){
        this.responsibleBody = []
        this.responsibleBody.push(new Type(xsitype,codeStr))
        this.responsibleBody.push(new Name(name, lang));

    }
}
module.exports = ResponsibleBody