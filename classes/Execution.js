
class Execution {
    constructor(eventObj, educationObj) {
        
        let edate = new Date(educationObj.slut_datum);
        edate.setFullYear(edate.getFullYear() );
        let month = edate.getMonth() +1;
        let date = edate.getDate();
        month = month < 10 ? `0${month}` : month;
        date = date < 10 ? `0${date}` : date;

        let dateStr = `${edate.getFullYear()}-${month}-${date}`
        let execution = eventObj.antagningsform == "Successiv" ? 3 : 1;
        this.execution =[];
        this.execution.push({'condition' : execution});

        if(eventObj.antagningsform == "Successiv"){
           
          
            this.execution.push({'end' : dateStr });
        
        } else {
            this.execution.push({'start': eventObj.start_datum});
            this.execution.push({'end' : eventObj.slut_datum});
            
        }
        

    }
};
module.exports = Execution