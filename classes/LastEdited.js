class LastEdited{   
    constructor(){
        let ledate = new Date();         
        ledate.setFullYear(ledate.getFullYear());
        let month = ledate.getMonth() +1;
        let date = ledate.getDate();
        month = month < 10 ? `0${month}` : month;
        date = date < 10 ? `0${date}` : date;
        let dateStr = `${ledate.getFullYear()}-${month}-${date}`             
        
    this.lastEdited = []
    this.lastEdited.push(dateStr)

    }

}
module.exports = LastEdited

