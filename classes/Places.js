const Attr = require("./Attr")
const simpleElement = require("./SimpleElement");

class Places{
    constructor(value, selectionGroup){
        this.places = []        
        let obj = new simpleElement('selectionGroup')
        obj.selectionGroup.push(new Attr({'xmlns:xsi':'http://www.w3.org/2001/XMLSchema-instance'},{'xsi:type':'C_SelectionGroup'},{'code':'totalt'}))
        this.places.push(obj)
        this.places.push(new simpleElement('plannedPlaces',value))
        
    }
}
module.exports = Places


