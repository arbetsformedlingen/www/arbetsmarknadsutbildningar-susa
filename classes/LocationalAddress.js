class LocationalAddress {
    constructor(town, municipalityCode, streetAddress, country, postCode, position ) {
    this.visitAddress = []
    this.visitAddress.push({'country' : 'SE'});
    this.visitAddress.push({'municipalityCode' : municipalityCode});
    this.visitAddress.push({'town' : town});
    this.visitAddress.push({'postCode' : postCode});    
    this.visitAddress.push({'streetAddress' : streetAddress});   
    this.visitAddress.push({'position' : position});
    
}
}
module.exports = LocationalAddress 