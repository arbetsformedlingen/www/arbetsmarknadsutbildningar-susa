const LangString = require("./LangString");
const Name = require("./Name");

class Type{
    constructor(xsitype, codeStr){
        this.type = {}
        this.type._attr= [];
        this.type._attr['xsi:type'] = xsitype;
        this.type._attr['code'] = codeStr;

    }
}
module.exports = Type