const EducationDescription = require("./EducationDescription");
const LangString = require("./LangString");
const LangUrl = require("./LangUrl");
const Form = require("./Form");
const EducationLevel = require("./EducationLevel");
const Subject = require("./Subject");
const simpleElement = require("./SimpleElement");
const Extent = require("./Extent");
const ResultIsDegree = require("./ResultIsDegree");
const Execution = require("./Execution");
const Identifier = require("./Identifier");
const Expires = require("./Expires")
const LastEdited = require("./LastEdited")
const Location = require("./Location");
const IdProvider = require("./IdProvider");
const IdEducation = require("./IdEducation");
const PaceOfStudy = require("./PaceOfStudy");
const Places = require("./Places");

class EducationEvent {
    constructor (educationObj,eventObj){
        this.educationEvent = [];
        this.educationEvent._attr = [];
        this.educationEvent._attr['xmlns:xsd'] = 'http://www.w3.org/2001/XMLSchema';
        this.educationEvent._attr['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';
        this.educationEvent._attr['xmlns'] = 'http://www.sis.se/ss10700/EMIL2.01';
        this.educationEvent.push(new Identifier("e",eventObj.tlr_utbildningstillfalle_id));
        this.educationEvent.push(new IdEducation("i",educationObj.tlr_unique_utbildning_id));
        this.educationEvent.push(new IdProvider("p",'1'));
        this.educationEvent.push(new LastEdited());
        this.educationEvent.push(new Expires());
        this.educationEvent.push(new Location(educationObj.postort,educationObj.adressrad,"swe",educationObj.longitude, educationObj.latitude, educationObj.kommun));
        this.educationEvent.push(new Execution(eventObj,educationObj));
        if(eventObj.antal_platser){
            this.educationEvent.push(new Places(eventObj.antal_platser));
        }
        if(eventObj.studietakt){
            this.educationEvent.push(new PaceOfStudy(eventObj.studietakt.replace('%', "")));
            
        }

           


    }   
    
}
module.exports = EducationEvent
