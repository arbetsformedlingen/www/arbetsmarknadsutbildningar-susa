const EducationDescription = require("./EducationDescription");
const LangString = require("./LangString");
const LangUrl = require("./LangUrl");
const Form = require("./Form");
const EducationLevel = require("./EducationLevel");
const Subject = require("./Subject");
const simpleElement = require("./SimpleElement")
const Extent = require("./Extent");
const ResultIsDegree = require("./ResultIsDegree");
const Identifier = require("./Identifier");
const Expires = require("./Expires")
const LastEdited = require("./LastEdited");
const IdProvider = require("./IdProvider");
const Extension = require("./Extension");
const Eligibility = require("./Eligibility");
const EligibilityDescription = require("./EligibilityDescription");
const IsVocational = require("./IsVocational");

class EducationInfo {
    constructor (educationObj){
        this.educationInfo = [];
        this.educationInfo._attr = [];
        this.educationInfo._attr['xmlns:xsd'] = 'http://www.w3.org/2001/XMLSchema';
        this.educationInfo._attr['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';
        this.educationInfo._attr['xmlns'] = 'http://www.sis.se/ss10700/EMIL2.01';
        this.educationInfo.push(new Identifier("i", educationObj.tlr_unique_utbildning_id));
        this.educationInfo.push(new LastEdited());
        this.educationInfo.push(new Expires());
        let title = {title : []};
        title.title.push(new LangString(educationObj.utbildningsnamn, 'swe'));
        this.educationInfo.push(title);
        this.educationInfo.push(new EducationDescription("<![CDATA["+educationObj.beskrivning+"]]>", 'swe'));
        this.educationInfo.push(new LangUrl(`https://arbetsformedlingen.se/for-arbetssokande/extra-stod/stod-a-o/arbetsmarknadsutbildning/hitta-arbetsmarknadsutbildning/yrkesomraden/aktiviteter/aktivitet/${educationObj.tlr_unique_utbildning_id}`,'swe'));
        this.educationInfo.push(new Form('C_OrganisationForm', 'AUB'));
        this.educationInfo.push(new EducationLevel());
        for(let i = 0;i <educationObj.ssyk_level_4_ssyk_code_2012.length; i++) {
            this.educationInfo.push(new Subject(educationObj.ssyk_level_4_ssyk_code_2012[i]));
        }
        if (educationObj.utbildningstyp === "Arbetsmarknadsutbildning")
            this.educationInfo.push(new IsVocational());
        
        this.educationInfo.push(new ResultIsDegree());
        this.educationInfo.push(new Extent(educationObj.riktlangd_dagar, 'days'));
        this.educationInfo.push(new Eligibility('För att kunna söka Arbetsmarknadsutbildningar, behöver du vara inskriven på Arbetsförmedlingen','swe'))        
        this.educationInfo.push(new Extension('AUBInfoExtension','0',educationObj.utbildningstyp, ));
    }   
       
}
module.exports = EducationInfo 
