const simpleElement = require("./SimpleElement");
const Attr = require("./Attr")
;
class Extent{
    constructor(value, unit){
        this.extent = []
        this.extent.push(new simpleElement('length',value))
        let obj = new simpleElement('unit')
        obj.unit.push(new Attr({'xsi:type':'C_TimeType'},{'code':unit}))
        this.extent.push(obj)
        
    }
}
module.exports = Extent
