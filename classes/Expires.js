
class Expires{
    constructor(){
        let edate = new Date();
        edate.setFullYear(edate.getFullYear() + 1);
        let month = edate.getMonth() +1;
        let date = edate.getDate();
        month = month < 10 ? `0${month}` : month;
        date = date < 10 ? `0${date}` : date;
        let dateStr = `${edate.getFullYear()}-${month}-${date}`
    this.expires = []
    this.expires.push(dateStr)

    }

}
module.exports = Expires

