

const ListItem = require("./ListItem");
const List = require("./List");
const ListBody = require("./ListBody");
const Head = require("./Head");

class EventList{
    constructor(eventList){
        
        let body = []
        let p = [];
        eventList.forEach(element => {
            
            p.push(new ListItem(element,`https://data.jobtechdev.se/utbildningar/utb_emil/${element.tlr_unique_utbildning_id.slice(0,3)}/${element.tlr_unique_utbildning_id}/events/${element.tlr_unique_tillfalle_id}.xml`))
        });
        body.push({p : p})
        this.html = []
        this.html._attr = [];
        this.html._attr['xmlns'] = 'http://www.w3.org/1999/xhtml';
        this.html._attr['xml:lang'] = 'sv';
        this.html._attr['lang'] = 'sv';
        this.html.push(new Head('EMIL 2.0 EventList'));
        this.html.p = [];
        this.html.push({body : body});



    }

}

module.exports = EventList





