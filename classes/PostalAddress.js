

class PostalAddress { 
    
    constructor(town, country, postCode, postBox, streetAddress, organization, department ){
        this.contactAddress = []
        this.contactAddress.push({'country' : 'SE'});
        this.contactAddress.push({'town' : town});        
        this.contactAddress.push({'postCode' : postCode});
        this.contactAddress.push({'postBox' : postBox});
        this.contactAddress.push({'streetAddress' : streetAddress});
        this.contactAddress.push({'organization' : organization});
        this.contactAddress.push({'department' : department});

    }
}
module.exports = PostalAddress
