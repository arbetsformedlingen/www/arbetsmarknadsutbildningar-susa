class Attr {
    constructor (...objs) {
        this._attr = []
        objs.forEach(obj => {
            Object.keys(obj).forEach(key => {
                this._attr[`${key}`]= obj[key]
            });
        });
    }

}
module.exports = Attr